from utils import *
from config import BUCKETS, paths

def main(**paths):

    #paths
    data_path = paths['data_path']
    catboost_path = paths['catboost_path']
    xgb_path = paths['xgb_path']
    lgbm_path = paths['lgbm_path']
    rf_path = paths['rf_path']
    stacking_path = paths['stacking_path']
    blending_path = paths['blending_path']

    #loading data and models
    data = pd.read_csv(data_path)
    cb_model = joblib.load(catboost_path)
    xgb_model = joblib.load(xgb_path)
    lgbm_model = joblib.load(lgbm_path)
    rf_model = joblib.load(rf_path)
    stacking_model = joblib.load(stacking_path)
    blending_model = joblib.load(blending_path)

    #folds
    data_train = data.loc[data['target'].notna()].copy()
    for col in data_train.select_dtypes(include='object').columns:
        data_train[col] = data_train[col].astype('category')
    exclude_cols = ['target', 'sk_id_curr']
    Y_train = data_train['target'].copy()
    X_train = data_train.drop(labels=exclude_cols, axis=1)
    X_train, X_test, y_train, y_test = train_test_split(X_train, Y_train, test_size=0.33, random_state=1)


    def get_auc(data):
        rocauc_control = roc_auc_score(data['target'], data['prediction_control'])
        rocauc_test = roc_auc_score(data['target'], data['prediction_test'])
        return rocauc_control, rocauc_test
    
    #different models

    """random forest"""

    #Приведение признаков
    catcols = []
    x_forest_train = X_train.copy()
    x_forest_test = X_test.copy()
    for col in x_forest_train.columns:
        if x_forest_train[col].dtype == 'category':
            catcols.append(col)

    encoder = OneHotEncoder()
    encoder_df = pd.DataFrame(encoder.fit_transform(x_forest_train[catcols]).toarray())
    encoder_df.columns = encoder.get_feature_names_out()

    x_forest_train.sort_index()
    x_forest_test.sort_index()

    x_forest_train.join(encoder_df)

    encoder_df = pd.DataFrame(encoder.fit_transform(x_forest_test[catcols]).toarray())
    encoder_df.columns = encoder.get_feature_names_out()
    x_forest_test.join(encoder_df)

    x_forest_test.drop(labels=catcols, axis=1, inplace=True)
    x_forest_train.drop(labels=catcols, axis=1, inplace=True)

    rf_model.fit(x_forest_train, y_train)

    """ XGB """

    cols_when_model_builds = xgb_model.get_booster().feature_names
    x_xgb_train = X_train[cols_when_model_builds]
    x_xgb_test = X_test[cols_when_model_builds]

    xgb_model.fit(x_xgb_train, y_train)

    """ catboost """

    x_cb_test = X_test.copy()
    x_cb_train = X_train.copy()

    feature_names = list(X_train.columns)
    cat_cols = []

    for col in x_cb_train.select_dtypes(include='category').columns:
        cat_cols.append(col)
        x_cb_test[col] = x_cb_test[col].astype(str)
        x_cb_train[col] = x_cb_train[col].astype(str)

    Xtr = x_cb_train.copy()
    Xtr[cat_cols] = Xtr[cat_cols].fillna('nan')
    cat_cols_idx = [feature_names.index(c) for c in cat_cols]
    Ytr = y_train.copy()
    xy = Pool(Xtr, Ytr, cat_features=cat_cols_idx)
    #cb_model.fit(xy)

    Xv = x_cb_test.copy()
    Xv[cat_cols] = Xv[cat_cols].fillna('nan')
    xx = Pool(Xv,cat_features=cat_cols_idx)


    #lgbm_model.fit(X_train, y_train)
    prediction = rf_model.predict_proba(x_forest_test)[:,0]


    df = pd.DataFrame()
    df['target'] = y_test
    df['id'] = range(y_test.shape[0])
    df['prediction_control'] = prediction

    #stacking_model.fit(X_train, y_train)
    prediction = xgb_model.predict_proba(x_xgb_test)[:,0]

    df['prediction_test'] = prediction

    df['bucket'] = pd.util.hash_pandas_object(df['id'], index=False) %BUCKETS
    tmp = df.groupby(['bucket']).apply(get_auc).reset_index().rename(columns={0:'rocauc'})
    tmp[['auc_control', 'auc_test']] = tmp['rocauc'].tolist()
    stat, pval = ttest_ind(tmp['auc_test'], tmp['auc_control'], equal_var=False)
    print('stat, p_value:', stat, round(pval,2))

main(**paths)
