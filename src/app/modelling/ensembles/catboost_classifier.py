from utils import *

def main(data_path, save_path):

    #Считывание данных
    data = pd.read_csv(data_path)

    #Готовим пул
    cat_cols = data.select_dtypes(include='object').columns
    exclude_cols = ['sk_id_curr', 'target']
    feature_names = set(data.columns) - set(exclude_cols)
    feature_names = list(feature_names)

    X_train = data.loc[data['target'].notna()].copy()
    X_train = X_train[feature_names].copy()
    Y_train = data.loc[data['target'].notna()]['target'].copy()

    for col in X_train.select_dtypes(include='object').columns:
        X_train[col] = X_train[col].astype(str)

    X = X_train.copy()
    Y = Y_train.copy()
    X[cat_cols] = X[cat_cols].fillna('nan')
    cat_cols_idx = [feature_names.index(c) for c in cat_cols]
    xy = Pool(X, Y, cat_features=cat_cols_idx)

    #Hyperopt
    space = {
    'learning_rate': hp.loguniform('learning_rate', -5, -1),
    'depth': hp.quniform('depth', 4, 10, 1),
    'iterations': 2000,
    'l2_leaf_reg': hp.loguniform('l2_leaf_reg', 0, 5),
    'bagging_temperature': hp.uniform('bagging_temperature', 0, 4),
    'random_strength': hp.uniform('random_strength', 0.2, 5),
    'eval_metric' : 'AUC',
    'loss_function' : "Logloss",
    }

    def objective(space):
        scores = cv(
            xy,
            space,
            iterations = 200,
            fold_count=5, 
            plot=False,
            early_stopping_rounds=100,
            logging_level = 'Silent',
        )
        return 1-scores['test-AUC-mean'].max()
    
    cb_best = {'bagging_temperature': 0.9550701349386217,
                'depth': 7.0,
                'l2_leaf_reg': 8.421239627246907,
                'learning_rate': 0.18711028845045238,
                'random_strength': 2.469587384775816
                }
    
    cb_params = {
    'eval_metric': 'AUC',
    'custom_metric': 'Accuracy',
    'loss_function': 'Logloss',
    'logging_level': 'Silent',
    'use_best_model': True,
    'iterations': 2000,
    }
    cb_params.update(cb_best)

    final_params = {'iterations': 510,
                    'bagging_temperature': 0.9550701349386217,
                    'depth': 7.0,
                    'l2_leaf_reg': 8.421239627246907,
                    'learning_rate': 0.18711028845045238,
                    'random_strength': 2.469587384775816
                    }
    
    cb_model = CatBoostClassifier(**final_params, verbose=False)

    #Обучение модели
    cb_model.fit(xy)
    
    #Сохраняем в pickle 
    joblib.dump(cb_model, save_path)


data_path = 'E:\\cft\\prepared_data_ensembles.csv'
save_path = 'E:\\cft\\catboost_classifier.pkl'

main(data_path=data_path, save_path=save_path)

