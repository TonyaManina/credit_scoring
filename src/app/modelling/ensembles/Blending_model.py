from utils import *
from config import paths

def main(**paths):

    #paths
    data_path = paths['data_path']
    catboost_path = paths['catboost_path']
    xgb_path = paths['xgb_path']
    lgbm_path = paths['lgbm_path']
    rf_path = paths['rf_path']
    save_path = paths['save_path']

    #loading data and models
    data = pd.read_csv(data_path)
    cb_model = joblib.load(catboost_path)
    xgb_model = joblib.load(xgb_path)
    lgbm_model = joblib.load(lgbm_path)
    rf_model = joblib.load(rf_path)

    #folds
    data_train = data.loc[data['target'].notna()].copy()
    for col in data_train.select_dtypes(include='object').columns:
        data_train[col] = data_train[col].astype('category')
    exclude_cols = ['target', 'sk_id_curr']
    Y_train = data_train['target'].copy()
    X_train = data_train.drop(labels=exclude_cols, axis=1)
    X_train_full, X_test, y_train_full, y_test = train_test_split(X_train, Y_train, test_size=0.33, random_state=1)
    x_train, x_val, y_train, y_val = train_test_split(X_train_full, y_train_full, test_size=0.33, random_state=1)

    """random forest"""

    #Приведение признаков
    catcols = []
    x_forest_train = x_train.copy()
    x_forest_val = x_val.copy()
    x_forest_test = X_test.copy()
    for col in x_forest_train.columns:
        if x_forest_train[col].dtype == 'category':
            catcols.append(col)

    encoder = OneHotEncoder()
    encoder_df = pd.DataFrame(encoder.fit_transform(x_forest_train[catcols]).toarray())
    encoder_df.columns = encoder.get_feature_names_out()

    x_forest_train.sort_index()
    x_forest_val.sort_index()
    x_forest_test.sort_index()

    x_forest_train.join(encoder_df)

    encoder_df = pd.DataFrame(encoder.fit_transform(x_forest_val[catcols]).toarray())
    encoder_df.columns = encoder.get_feature_names_out()
    x_forest_val.join(encoder_df)

    encoder_df = pd.DataFrame(encoder.fit_transform(x_forest_test[catcols]).toarray())
    encoder_df.columns = encoder.get_feature_names_out()
    x_forest_test.join(encoder_df)

    x_forest_test.drop(labels=catcols, axis=1, inplace=True)
    x_forest_train.drop(labels=catcols, axis=1, inplace=True)
    x_forest_val.drop(labels=catcols, axis=1, inplace=True)

    rf_model.fit(x_forest_train, y_train)
    y_hat_forest = rf_model.predict(x_forest_val)

    """ XGB """
    cols_when_model_builds = xgb_model.get_booster().feature_names
    x_xgb_train = x_train[cols_when_model_builds]
    x_xgb_test = X_test[cols_when_model_builds]
    x_xgb_val = x_val[cols_when_model_builds]

    xgb_model.fit(x_xgb_train, y_train)
    y_hat_xgb = xgb_model.predict(x_xgb_val)

    """ CatBoost """
    x_cb_val = x_val.copy()
    x_cb_test = X_test.copy()
    x_cb_train = x_train.copy()

    feature_names = list(x_train.columns)
    cat_cols = []

    for col in x_cb_val.select_dtypes(include='category').columns:
        cat_cols.append(col)
        x_cb_val[col] = x_cb_val[col].astype(str)
        x_cb_test[col] = x_cb_test[col].astype(str)
        x_cb_train[col] = x_cb_train[col].astype(str)

    Xtr = x_cb_train.copy()
    Xtr[cat_cols] = Xtr[cat_cols].fillna('nan')
    cat_cols_idx = [feature_names.index(c) for c in cat_cols]
    Ytr = y_train.copy()
    xy = Pool(Xtr, Ytr, cat_features=cat_cols_idx)
    cb_model.fit(xy)

    Xv = x_cb_val.copy()
    Xv[cat_cols] = Xv[cat_cols].fillna('nan')
    xx = Pool(Xv,cat_features=cat_cols_idx)
    y_hat_cb = cb_model.predict(xx)

    """ Fitting ensemble """
    meta_X = []
    y_hat_cb = y_hat_cb.reshape(len(y_hat_cb), 1)
    meta_X.append(y_hat_cb)
    y_hat_forest = y_hat_forest.reshape(len(y_hat_forest), 1)
    meta_X.append(y_hat_forest)
    y_hat_xgb = y_hat_xgb.reshape(len(y_hat_xgb), 1)
    meta_X.append(y_hat_xgb)

    meta_X = np.hstack(meta_X)
    blender_model = lgbm_model
    blender_model.fit(meta_X, y_val)

    """ Scores """
    Xte = x_cb_test.copy()
    Xte[cat_cols] = Xte[cat_cols].fillna('nan')
    xxx = Pool(Xte, cat_features=cat_cols_idx)
    y_hat_test_cb = cb_model.predict(xxx)

    y_hat_test_forest = rf_model.predict(x_forest_test)

    y_hat_test_xgb = xgb_model.predict(x_xgb_test)

    meta_X = []
    y_hat_test_cb = y_hat_test_cb.reshape(len(y_hat_test_cb), 1)
    meta_X.append(y_hat_test_cb)
    y_hat_test_forest = y_hat_test_forest.reshape(len(y_hat_test_forest), 1)
    meta_X.append(y_hat_test_forest)
    y_hat_test_xgb = y_hat_test_xgb.reshape(len(y_hat_test_xgb), 1)
    meta_X.append(y_hat_test_xgb)

    meta_X = np.hstack(meta_X)
    print('ROC AUC score on test:', roc_auc_score(y_test, blender_model.predict_proba(meta_X)[:, 1]))

    joblib.dump(blender_model, save_path)

del paths['stacking_path']
del paths['blending_path']
paths['save_path'] = 'E:\\cft\\blending_classifier.pkl'

main(**paths)
