from utils import *

def main(data_path, save_path):

    #Считывание данных
    data = pd.read_csv(data_path)

    cat_cols = data.select_dtypes(include='object').columns
    exclude_cols = ['sk_id_curr', 'target']
    feature_names = set(data.columns) - set(exclude_cols)
    feature_names = list(feature_names)

    X_train = data.loc[data['target'].notna()].copy()
    X_train = X_train[feature_names].copy()
    Y_train = data.loc[data['target'].notna()]['target'].copy()

    for col in X_train.select_dtypes(include='object').columns:
        X_train[col] = X_train[col].astype('category')

    space = {
        'n_estimators': hp.randint('n_estimators',100,500),
        'max_depth': hp.randint('max_depth', 5, 20),
        'learning_rate': hp.uniform('learning_rate', 0.01, 0.1),
        'enable_categorical': True,
    }

    def objective(space, x_train, y_train):
        xgb_clf = xgb.XGBClassifier(**space)
        skf = StratifiedKFold(n_splits=5, shuffle=True, random_state=1)
        score = cross_val_score(estimator=xgb_clf, X=x_train, y=y_train, scoring='roc_auc', cv=skf)
        return {'loss': -score.mean(), 'params': space, 'status': STATUS_OK}
    
    trials = Trials()

    best = {'learning_rate': 0.05809765211822434, 'max_depth': 5, 'n_estimators': 304}
    params = {
        'n_estimators': 304,
        'max_depth': 5,
        'learning_rate': 0.5,
        'enable_categorical': True,
        }
    
    params.update(best)
    xgb_model = xgb.XGBClassifier(**params)
    xgb_model.fit(X_train, Y_train)

    #Сохраняем в pickle 
    joblib.dump(xgb_model, save_path)


data_path = 'E:\\cft\\prepared_data_ensembles.csv'
save_path = 'E:\\cft\\xgb_classifier.pkl'

main(data_path=data_path, save_path=save_path)
