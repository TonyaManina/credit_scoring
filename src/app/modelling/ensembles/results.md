# Statistically significant differences

|  p_value    | lgbm | catboost | random forest | stacking | xgb |
|-------------|------|----------|---------------|----------|-----|
| lgbm        |   1  |   0.49   |       0       |    0.44  | 0.93|
| catboost    | 0.49 |     1    |       0       |    0.92  | 0.45|
|random forest|  0   |     0    |       1       |     0    |  0  |
|stacking     | 0.44 |   0.45   |       0       |     1    | 0.4 |
|xgb          | 0.93 |   0.45   |       0       |     0.4  |  1  |


# ROC-AUC scores on test

- CatBoost 0.7710328770543033
- blending 0.686263393776457
- xgboost 0.773305172408341
- random forest 0.7500313438132795
- lgbm 0.7731119318783279
- stacking_model 0.7707526581453921