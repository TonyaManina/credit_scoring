from utils import *

def main(data_path, save_path):

    #Считывание данных
    data = pd.read_csv(data_path)

    #обработка категориальных признаков с помощью one hot encoder
    catcols = []
    for col in data.columns:
        if data[col].dtype == 'object':
            catcols.append(col)

    encoder = OneHotEncoder()
    encoder_df = pd.DataFrame(encoder.fit_transform(data[catcols]).toarray())
    encoder_df.columns = encoder.get_feature_names_out()

    data = data.join(encoder_df)
    data.drop(labels=catcols, axis=1, inplace=True)

    #подготовка train датасета
    x_train = data.loc[data['target'].notna()].copy()
    exclude_cols = ['target', 'sk_id_curr']
    x_train.drop(labels=exclude_cols, axis=1, inplace=True)
    y_train = data.loc[data['target'].notna()]['target']

    space = {
    'min_samples_leaf': hp.randint('min_samples_leaf', 1, 10),
    'n_estimators': hp.randint('n_estimators',25, 500),
    'max_depth': hp.randint('max_depth',1, 10),
    'class_weight': 'balanced',
    }

    def objective(space, x_train, y_train):
        rt_clf = RandomForestClassifier(**space)
        skf = StratifiedKFold(n_splits=5, shuffle=True, random_state=1)
        score = cross_val_score(estimator=rt_clf, X=x_train, y=y_train, scoring='roc_auc', cv=skf)
        return {'loss': -score.mean(), 'params': space, 'status': STATUS_OK}

    best = {'max_depth': 9, 'min_samples_leaf': 4, 'n_estimators': 496}

    params = {
        'n_estimators': 304,
        'max_depth': 5,
        'min_samples_leaf': 0.5,
        'class_weight': 'balanced',
    }

    params.update(best)
    rf_model = RandomForestClassifier(**params)
    rf_model.fit(x_train, y_train)

    #Сохраняем в pickle 
    joblib.dump(rf_model, save_path)

data_path = 'E:\\cft\\prepared_data_ensembles.csv'
save_path = 'E:\\cft\\rf_classifier.pkl'

main(data_path=data_path, save_path=save_path)
