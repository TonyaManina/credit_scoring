from utils import *

def main(data_path, save_path):

    #Считывание данных
    data = pd.read_csv(data_path)

    cat_cols = data.select_dtypes(include='object').columns
    exclude_cols = ['sk_id_curr', 'target']
    feature_names = set(data.columns) - set(exclude_cols)
    feature_names = list(feature_names)

    X_train = data.loc[data['target'].notna()].copy()
    X_train = X_train[feature_names].copy()
    Y_train = data.loc[data['target'].notna()]['target'].copy()

    for col in X_train.select_dtypes(include='object').columns:
        X_train[col] = X_train[col].astype('category')

    space = {
        'num_leaves': hp.randint('num_leaves', 5, 130),
        'n_estimators': 100+hp.randint('n_estimators',500),
        'max_depth': 5+hp.randint('max_depth', 20),
        'learning_rate': hp.uniform('learning_rate', 0.01, 0.1),
        'colsample_bytree': hp.uniform('colsample_bytree', 0.3, 1.0),
        }
    
    def objective(space, x_train, y_train):
        lgb_clf = lgbm.LGBMClassifier(**space)
        skf = StratifiedKFold(n_splits=5, shuffle=True, random_state=1)
        score = cross_val_score(estimator=lgb_clf, X=x_train, y=y_train, scoring='roc_auc', cv=skf)
        return {'loss': -score.mean(), 'params': space, 'status': STATUS_OK}

    best = {'colsample_bytree': 0.34867889313484324,
        'learning_rate': 0.03402264358031887,
        'max_depth': 17,
        'n_estimators': 199,
        'num_leaves': 104}
    
    params = {
    'n_estimators': 304,
    'max_depth': 5,
    'learning_rate': 0.5,
    'num_leaves': 104,
    'colsample_bytree': 5,
    }
    params.update(best)

    lgbm_clf = lgbm.LGBMClassifier(**params)
    lgbm_clf.fit(X_train, Y_train)

    #Сохраняем в pickle 
    joblib.dump(lgbm_clf, save_path)


data_path = 'E:\\cft\\prepared_data_ensembles.csv'
save_path = 'E:\\cft\\lgbm_classifier.pkl'

main(data_path=data_path, save_path=save_path)
