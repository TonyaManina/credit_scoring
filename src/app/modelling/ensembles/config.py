BUCKETS = 50
paths = {'data_path' : 'E:\\cft\\prepared_data_ensembles.csv',
         'catboost_path' : 'E:\\cft\\catboost_classifier.pkl',
         'xgb_path' : 'E:\\cft\\xgb_classifier.pkl',
         'lgbm_path' : 'E:\\cft\\lgbm_classifier.pkl',
         'rf_path': 'E:\\cft\\rf_classifier.pkl',
         'stacking_path': 'E:\\cft\\stacking_classifier.pkl',
         'blending_path': 'E:\\cft\\blending_classifier.pkl'}
