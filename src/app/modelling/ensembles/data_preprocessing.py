import pandas as pd
import numpy as np
from sklearn.preprocessing import OneHotEncoder

def highly_correlated_cols(df):
    #возвращает сильно коррелированные столбцы (0.9 - пороговое значение)
    #используется корреляция Пирсона
    correlation_matrix = df.select_dtypes(include=np.number).corr().abs()
    up = correlation_matrix.where(np.triu(np.ones(correlation_matrix.shape), k=1).astype(np.bool_))
    highly_corr_cols = [col for col in up.columns if any(up[col]>0.9)]
    return highly_corr_cols


def main(data_path, save_path):
    #объединенные таблицы application и сгенерированные важные признаки других
    #таблиц в дз4
    data = pd.read_csv(data_path)

    #Удаление мультиколлинеарных столбцов
    to_drop = highly_correlated_cols(data)
    data.drop(labels=to_drop, axis=1, inplace=True)

    #Заполнение пропусков
    for col in data.columns:
        if col != 'TARGET' and data[col].dtype != 'object':
            data[col].fillna(data[col].median(), inplace=True)

    #Приведение к одинаковому стилю всех названий
    data.columns = data.columns.str.replace(' ', '_')
    data.columns = data.columns.str.lower()

    # save prepared dataset
    data.to_csv(save_path, index=False)

data_path = 'E:\\cft\\important_features.csv'
save_path = 'E:\\cft\\prepared_data_ensembles.csv'

main(data_path=data_path,save_path=save_path)
