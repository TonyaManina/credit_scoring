import pandas as pd
import joblib
from sklearn.ensemble import StackingClassifier

from config import paths

def main(**paths):

    #paths
    data_path = paths['data_path']
    catboost_path = paths['catboost_path']
    xgb_path = paths['xgb_path']
    lgbm_path = paths['lgbm_path']
    save_path = paths['save_path']

    #loading data and models
    data = pd.read_csv(data_path)
    cb_model = joblib.load(catboost_path)
    xgb_model = joblib.load(xgb_path)
    lgbm_model = joblib.load(lgbm_path)

    #folds
    data_train = data.loc[data['target'].notna()].copy()
    for col in data_train.select_dtypes(include='object').columns:
        data_train[col] = data_train[col].astype('category')

    exclude_cols = ['target', 'sk_id_curr']
    Y_train = data_train['target'].copy()
    X_train = data_train.drop(labels=exclude_cols, axis=1)   

    #Stacking
    estimators = [
    ('xgb', xgb_model),
    ('lgbm', lgbm_model)
    ]

    stacking_model = StackingClassifier(estimators=estimators, final_estimator=cb_model)
    stacking_model.fit(X_train, Y_train)

    joblib.dump(stacking_model, save_path)

del paths['rf_path']
del paths['stacking_path']
del paths['blending_path']
paths['save_path'] = 'E:\\cft\\stacking_classifier.pkl'

main(**paths)
