import pandas as pd
import numpy as np
import pickle
import joblib
from functools import partial
import xgboost as xgb
from sklearn.model_selection import StratifiedKFold, cross_val_score
from hyperopt import hp, fmin, tpe, STATUS_OK, Trials
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import train_test_split
from scipy.stats import ttest_ind
from sklearn.preprocessing import OneHotEncoder
from catboost import Pool, CatBoostClassifier, cv
from sklearn.ensemble import StackingClassifier
from functools import partial
import lightgbm as lgbm
from sklearn.ensemble import RandomForestClassifier