import pandas as pd

from utils import Mann_Whitney_significance, Chi2_significance, Ordinal_Encoding

def main(df_path, target_path, save_path):
    bureau = pd.read_csv(df_path)
    target = pd.read_csv(target_path, usecols=['TARGET','SK_ID_CURR'])
    bureau = bureau.merge(target, on = 'SK_ID_CURR')

    bool_cols = [col for col in bureau.columns if bureau[col].nunique() == 2]
    bool_cols.remove('TARGET')

    cat_cols = bureau.select_dtypes(include='object').columns
    cat_cols = list(set(cat_cols) - set(bool_cols))

    num_cols = list(set(bureau.columns) - set(cat_cols) -
                    set(bool_cols) - {'TARGET','SK_ID_CURR'})
    
    for col in cat_cols:
        mode = bureau[col].mode().values[0]
        bureau[col].fillna(mode,inplace=True)
        bureau[col] = Ordinal_Encoding(bureau, col)
        bureau = Chi2_significance(bureau,col)

    for col in bool_cols:
        bureau = Chi2_significance(bureau, col)

    for col in num_cols:
        medi = bureau[col].median()
        bureau[col] = bureau[col].fillna(medi)
        bureau = Mann_Whitney_significance(bureau, col)

    bureau.drop(labels='TARGET',axis=1,inplace=True)

    bureau.to_csv(save_path, index=False)

df_path = 'E:\\home-credit-default-risk\\bureau.csv'
target_path = 'E:\\home-credit-default-risk\\application_train.csv'
save_path = 'E:\\home-credit-default-risk\\bureau_sign_fea.csv'

main(df_path=df_path,target_path=target_path,save_path=save_path)

