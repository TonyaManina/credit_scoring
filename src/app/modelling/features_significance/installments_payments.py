import pandas as pd

from utils import Mann_Whitney_significance, Chi2_significance, Ordinal_Encoding

def main(df_path, target_path, save_path):
    ip = pd.read_csv(df_path)
    target = pd.read_csv(target_path, usecols=['TARGET','SK_ID_CURR'])
    ip = ip.merge(target, on = 'SK_ID_CURR')

    bool_cols = [col for col in ip.columns if ip[col].nunique() == 2]
    bool_cols.remove('TARGET')

    cat_cols = ip.select_dtypes(include='object').columns
    cat_cols = list(set(cat_cols) - set(bool_cols))

    num_cols = list(set(ip.columns) - set(cat_cols) -
                    set(bool_cols) - {'TARGET','SK_ID_CURR', 'SK_ID_PREV'})
    
    for col in cat_cols:
        mode = ip[col].mode().values[0]
        ip[col].fillna(mode,inplace=True)
        ip[col] = Ordinal_Encoding(ip, col)
        ip = Chi2_significance(ip,col)

    for col in bool_cols:
        ip = Chi2_significance(ip, col)

    for col in num_cols:
        medi = ip[col].median()
        ip[col] = ip[col].fillna(medi)
        ip = Mann_Whitney_significance(ip, col)

    ip.drop(labels='TARGET',axis=1,inplace=True)

    ip.to_csv(save_path, index=False)

df_path = 'E:\home-credit-default-risk\old_installment_payments.csv'
target_path = 'E:\\home-credit-default-risk\\application_train.csv'
save_path = 'E:\\home-credit-default-risk\\installments_payments_sign_fea.csv'

main(df_path=df_path,target_path=target_path,save_path=save_path)