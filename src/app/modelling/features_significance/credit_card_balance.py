import pandas as pd

from utils import Mann_Whitney_significance, Chi2_significance, Ordinal_Encoding

def main(df_path, target_path, save_path):

    ccb = pd.read_csv(df_path)
    target = pd.read_csv(target_path, usecols=['TARGET','SK_ID_CURR'])
    ccb = ccb.merge(target, on = 'SK_ID_CURR')

    bool_cols = [col for col in ccb.columns if ccb[col].nunique() == 2]
    bool_cols.remove('TARGET')

    cat_cols = ccb.select_dtypes(include='object').columns
    cat_cols = list(set(cat_cols) - set(bool_cols))

    num_cols = list(set(ccb.columns) - set(cat_cols) -
                    set(bool_cols) - {'TARGET','SK_ID_CURR', 'SK_ID_PREV'})
    
    for col in cat_cols:
        mode = ccb[col].mode().values[0]
        ccb[col].fillna(mode,inplace=True)
        ccb[col] = Ordinal_Encoding(ccb, col)
        ccb = Chi2_significance(ccb,col)

    for col in bool_cols:
        ccb = Chi2_significance(ccb, col)

    for col in num_cols:
        medi = ccb[col].median()
        ccb[col] = ccb[col].fillna(medi)
        ccb = Mann_Whitney_significance(ccb, col)

    ccb.drop(labels='TARGET',axis=1,inplace=True)

    ccb.to_csv(save_path, index=False)

df_path = 'E:\home-credit-default-risk\old_credit_card_balance.csv'
target_path = 'E:\\home-credit-default-risk\\application_train.csv'
save_path = 'E:\\home-credit-default-risk\\credit_card_balance_sign_fea.csv'

main(df_path=df_path, target_path=target_path, save_path=save_path)