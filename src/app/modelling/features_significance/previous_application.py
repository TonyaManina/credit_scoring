import pandas as pd

from utils import Mann_Whitney_significance, Chi2_significance, Ordinal_Encoding

def main(df_path, target_path, save_path):
    prev_application = pd.read_csv(df_path)
    target = pd.read_csv(target_path, usecols=['TARGET','SK_ID_CURR'])
    prev_application = prev_application.merge(target, on = 'SK_ID_CURR')

    bool_cols = [col for col in prev_application.columns if prev_application[col].nunique() == 2]
    bool_cols.remove('TARGET')

    cat_cols = prev_application.select_dtypes(include='object').columns
    cat_cols = list(set(cat_cols) - set(bool_cols))

    num_cols = list(set(prev_application.columns) - set(cat_cols) -
                    set(bool_cols) - {'TARGET','SK_ID_CURR', 'SK_ID_PREV'})
    
    for col in cat_cols:
        mode = prev_application[col].mode().values[0]
        prev_application[col].fillna(mode,inplace=True)
        prev_application[col] = Ordinal_Encoding(prev_application, col)
        prev_application = Chi2_significance(prev_application,col)

    for col in bool_cols:
        prev_application = Chi2_significance(prev_application, col)

    for col in num_cols:
        medi = prev_application[col].median()
        prev_application[col] = prev_application[col].fillna(medi)
        prev_application = Mann_Whitney_significance(prev_application, col)

    prev_application.drop(labels='TARGET',axis=1,inplace=True)

    prev_application.to_csv(save_path, index=False)


df_path = 'E:\home-credit-default-risk\previous_application.csv'
target_path = 'E:\\home-credit-default-risk\\application_train.csv'
save_path = 'E:\\home-credit-default-risk\\previous_application_sign_fea.csv'

main(df_path=df_path,target_path=target_path,save_path=save_path)