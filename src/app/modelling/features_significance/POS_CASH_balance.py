import pandas as pd

from utils import Mann_Whitney_significance, Chi2_significance, Ordinal_Encoding

def main(df_path, target_path, save_path):
    pos_cash = pd.read_csv(df_path)
    target = pd.read_csv(target_path, usecols=['TARGET','SK_ID_CURR'])
    pos_cash = pos_cash.merge(target, on = 'SK_ID_CURR')

    bool_cols = [col for col in pos_cash.columns if pos_cash[col].nunique() == 2]
    bool_cols.remove('TARGET')

    cat_cols = pos_cash.select_dtypes(include='object').columns
    cat_cols = list(set(cat_cols) - set(bool_cols))

    num_cols = list(set(pos_cash.columns) - set(cat_cols) -
                    set(bool_cols) - {'TARGET','SK_ID_CURR', 'SK_ID_PREV'})
    
    for col in cat_cols:
        mode = pos_cash[col].mode().values[0]
        pos_cash[col].fillna(mode,inplace=True)
        pos_cash[col] = Ordinal_Encoding(pos_cash, col)
        pos_cash = Chi2_significance(pos_cash,col)

    for col in bool_cols:
        pos_cash = Chi2_significance(pos_cash, col)

    for col in num_cols:
        medi = pos_cash[col].median()
        pos_cash[col] = pos_cash[col].fillna(medi)
        pos_cash = Mann_Whitney_significance(pos_cash, col)

    pos_cash.drop(labels='TARGET',axis=1,inplace=True)

    pos_cash.to_csv(save_path, index=False)

df_path = 'E:\home-credit-default-risk\old_POS_CASH.csv'
target_path = 'E:\\home-credit-default-risk\\application_train.csv'
save_path = 'E:\\home-credit-default-risk\\POS_CASH_balance_sign_fea.csv'

main(df_path=df_path,target_path=target_path,save_path=save_path)