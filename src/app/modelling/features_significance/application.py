import pandas as pd

from utils import Mann_Whitney_significance, Chi2_significance, Ordinal_Encoding, bootstraping_mean, verdict

def main(df_path, save_path):

    application = pd.read_csv(df_path)
    #Я разделяю колонки на категориальные+бинарные и все остальные, для
    #первых буду использовать Хи-квадрат, для остальных Манна-Уитни
    bool_cols = [col for col in application.columns if application[col].nunique() == 2]
    bool_cols.remove('TARGET')

    cat_cols = application.select_dtypes(include='object').columns
    cat_cols = list(set(cat_cols) - set(bool_cols))

    num_cols = list(set(application.columns) - set(cat_cols) -
                    set(bool_cols) - {'TARGET','SK_ID_CURR'})
    
    for col in cat_cols:
        mode = application[col].mode().values[0]
        application[col].fillna(mode,inplace=True)
        application[col] = Ordinal_Encoding(application, col)
        application = Chi2_significance(application,col)

    for col in bool_cols:
        application = Chi2_significance(application, col)

    for col in num_cols:
        medi = application[col].median()
        application[col] = application[col].fillna(medi)
        application = Mann_Whitney_significance(application, col)

    application.to_csv(save_path, index=False)

def main_bootstrap(df_path, save_path):

    application = pd.read_csv(df_path)
    bool_cols = [col for col in application.columns if application[col].nunique() == 2]
    bool_cols.remove('TARGET')

    cat_cols = application.select_dtypes(include='object').columns
    cat_cols = list(set(cat_cols) - set(bool_cols))

    num_cols = list(set(application.columns) - set(cat_cols) -
                    set(bool_cols) - {'TARGET','SK_ID_CURR'})
    
    for col in cat_cols:
        mode = application[col].mode().values[0]
        application[col].fillna(mode,inplace=True)
        application[col] = Ordinal_Encoding(application, col)
        application = Chi2_significance(application,col)

    for col in bool_cols:
        application = Chi2_significance(application, col)

    #bootstrap
    important_features = []
    for col in num_cols:
        application[col] = application[col].fillna(application[col].median())
        cidiff = bootstraping_mean(application, application['TARGET'], feat_name=col)
        if verdict(cidiff) == 1:
            important_features.append(col)
    
    to_drop = []
    for col in application.columns:
        if (col not in bool_cols) and (col not in cat_cols):
            if col not in important_features:
                to_drop.append(col)

    application.drop(labels=to_drop,axis=1,inplace=True)
    application.to_csv(save_path,index=False)

df_path = 'E:\\home-credit-default-risk\\application_train.csv'
save_path_boot = 'E:\\home-credit-default-risk\\application_sign_fea_bootstrap.csv'
save_path_mw = 'E:\\home-credit-default-risk\\application_sign_fea_mw.csv'

main(df_path=df_path, save_path=save_path_mw)
#main_bootstrap(df_path=df_path, save_path=save_path_boot)