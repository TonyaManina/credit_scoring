import pandas as pd

from utils import Mann_Whitney_significance, Chi2_significance, Ordinal_Encoding

def main(df_path, bureau_path, target_path, save_path):

    bb = pd.read_csv(df_path)
    bureau = pd.read_csv(bureau_path, usecols=['SK_ID_CURR', 'SK_ID_BUREAU'])
    target = pd.read_csv(target_path, usecols=['TARGET','SK_ID_CURR'])
    bb = bb.merge(bureau, on = 'SK_ID_BUREAU')
    bb = bb.merge(target, on = 'SK_ID_CURR')

    bool_cols = [col for col in bb.columns if bb[col].nunique() == 2]
    bool_cols.remove('TARGET')

    cat_cols = bb.select_dtypes(include='object').columns
    cat_cols = list(set(cat_cols) - set(bool_cols))

    num_cols = list(set(bb.columns) - set(cat_cols) -
                    set(bool_cols) - {'TARGET','SK_ID_CURR','SK_ID_BUREAU'})
    
    for col in cat_cols:
        mode = bb[col].mode().values[0]
        bb[col].fillna(mode,inplace=True)
        bb[col] = Ordinal_Encoding(bb, col)
        bb = Chi2_significance(bb,col)

    for col in bool_cols:
        bb = Chi2_significance(bb, col)

    for col in num_cols:
        medi = bb[col].median()
        bb[col] = bb[col].fillna(medi)
        bb = Mann_Whitney_significance(bb, col)

    bb.drop(labels='TARGET',axis=1,inplace=True)

    bb.to_csv(save_path, index=False)

df_path = 'E:\\home-credit-default-risk\\old_bureau_balance.csv'
bureau_path = 'E:\\home-credit-default-risk\\bureau.csv'
target_path = 'E:\\home-credit-default-risk\\application_train.csv'
save_path = 'E:\\home-credit-default-risk\\bureau_balance_sign_fea.csv'

main(df_path=df_path,bureau_path=bureau_path,target_path=target_path,save_path=save_path)