import numpy as np
import pandas as pd

#Пути до файлов
path_read = 'E:\\home-credit-default-risk\\application.csv'
path_save = 'E:\\home-credit-default-risk\\application_features.csv'

def main(path_read,path_save):

    #Preprocessing
    application = pd.read_csv(path_read)
    application_features = pd.DataFrame()
    application_features['SK_ID_CURR'] = application['SK_ID_CURR']

    #1. Число документов (+1 за паспорт)
    documents = []
    for i in range(2,22):
        documents.append(f'FLAG_DOCUMENT_{i}')

    application_features['NUM_DOCS'] = application[documents].sum(axis=1)+1

    #2. House info
    house_info = ['APARTMENTS_AVG','BASEMENTAREA_AVG','YEARS_BEGINEXPLUATATION_AVG',
                  'YEARS_BUILD_AVG','COMMONAREA_AVG','ELEVATORS_AVG','ENTRANCES_AVG',
                  'FLOORSMAX_AVG','FLOORSMIN_AVG','LANDAREA_AVG','LIVINGAPARTMENTS_AVG',
                  'LIVINGAREA_AVG','NONLIVINGAPARTMENTS_AVG','NONLIVINGAREA_AVG',
                  'APARTMENTS_MODE','BASEMENTAREA_MODE','YEARS_BEGINEXPLUATATION_MODE',
                  'YEARS_BUILD_MODE','COMMONAREA_MODE','ELEVATORS_MODE','ENTRANCES_MODE',
                  'FLOORSMAX_MODE','FLOORSMIN_MODE','LANDAREA_MODE','LIVINGAPARTMENTS_MODE',
                  'LIVINGAREA_MODE','NONLIVINGAPARTMENTS_MODE','NONLIVINGAREA_MODE','APARTMENTS_MEDI',
                  'BASEMENTAREA_MEDI','YEARS_BEGINEXPLUATATION_MEDI','YEARS_BUILD_MEDI','COMMONAREA_MEDI',
                  'ELEVATORS_MEDI','ENTRANCES_MEDI','FLOORSMAX_MEDI','FLOORSMIN_MEDI','LANDAREA_MEDI',
                  'LIVINGAPARTMENTS_MEDI','LIVINGAREA_MEDI','NONLIVINGAPARTMENTS_MEDI',
                  'NONLIVINGAREA_MEDI','FONDKAPREMONT_MODE','HOUSETYPE_MODE','TOTALAREA_MODE',
                  'WALLSMATERIAL_MODE','EMERGENCYSTATE_MODE']
    
    #в 2 раза быстрее apply
    application_features['HOUSE_INFO'] = np.where(application[house_info].isnull().sum(axis=1) < 30, 1, 0)

    #3. Age (years)
    application_features['AGE_YRS'] = application['DAYS_BIRTH']//-365

    #4. Year change doc
    application_features['DOC_CHANGE_YEAR'] = (application['DAYS_BIRTH'] - application['DAYS_ID_PUBLISH'])//-365

    #5. Delay feature
    #Пятый признак оказался не совсем нужен, поэтому шестой сразу считаю, вы в чате написали что так можно :)
    application_features['FLAG_DELAY_DOC_CHANGE']  = np.where((application_features['DOC_CHANGE_YEAR']==14)|
                                                              (application_features['DOC_CHANGE_YEAR']==20)|
                                                              (application_features['DOC_CHANGE_YEAR']==45), 0, 1)

    #6. annuity/income
    application_features['ANNUITY_INCOME_RATIO'] = application['AMT_ANNUITY']/application['AMT_INCOME_TOTAL']

    #7. children per adult
    application_features['CHILDREN_PER_ADULT'] =  application['CNT_CHILDREN']/(application['CNT_FAM_MEMBERS'] - application['CNT_CHILDREN'])

    #8. average income per child
    #Если детей нет то заполняю пропуском
    application_features['INCOME_PER_CHILD'] = application['AMT_INCOME_TOTAL']/application['CNT_CHILDREN']
    application_features.loc[application_features['INCOME_PER_CHILD'] == float('inf'),'INCOME_PER_CHILD'] = np.nan

    #9. average income per adult
    application_features['INCOME_PER_ADULT'] = application['AMT_INCOME_TOTAL']/(application['CNT_FAM_MEMBERS']-application['CNT_CHILDREN'])

    #10. Interest rate
    application_features['INTEREST_RATE'] = (application['AMT_CREDIT']/application['AMT_GOODS_PRICE'])*100 - 100

    #11. score from external sources
    application_features['SCORE_EXT_SOURCE'] = application[['EXT_SOURCE_1','EXT_SOURCE_2','EXT_SOURCE_3']].mean(axis=1)

    #12. Groups by sex and education
    #теперь средний доход считаю на трейне
    mean_income = application.loc[application['TARGET'].notna()].groupby(['CODE_GENDER','NAME_EDUCATION_TYPE'])['AMT_INCOME_TOTAL'].transform('mean')
    application_features['DIFFERENCE_BTW_INCOME_AND_MEAN_GROUP_INCOME'] = application['AMT_INCOME_TOTAL']-mean_income


    application_features.set_index('SK_ID_CURR',inplace=True)
    application_features.to_csv(path_save,sep=',')

main(path_read,path_save)

