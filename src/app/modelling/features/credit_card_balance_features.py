import pandas as pd

#Пути до файлов
path_read = 'E:\\home-credit-default-risk\\old_credit_card_balance.csv'
path_save = 'E:\\home-credit-default-risk\\credit_card_balance_features.csv'

def main(path_read,path_save):

    #Preprocessing
    credit_card_balance = pd.read_csv(path_read)
  
    #All aggregates
    aggs = ['sum', 'mean', 'median', 'min', 'max', 'std', 'var', 'mad', 'prod']
    cols = ['AMT_BALANCE','AMT_CREDIT_LIMIT_ACTUAL']

    aggs_cols = credit_card_balance.groupby('SK_ID_PREV')[cols].agg(['sum', 'mean', 'median', 'min', 'max', 'std', 'var', 'prod'])
    aggs_cols.columns = [f"{col[0]}_{col[1]}" for col in aggs_cols.columns]

    #aggs change
    aggs_cols_3m = credit_card_balance.loc[credit_card_balance['MONTHS_BALANCE'] <= -3].groupby('SK_ID_PREV')[cols].agg(['sum', 'mean', 'median', 'min', 'max', 'std', 'var', 'prod'])
    aggs_cols_3m.columns = [f"{col[0]}_{col[1]}" for col in aggs_cols_3m.columns]

    difference = aggs_cols/aggs_cols_3m
    difference = difference.add_suffix('_diff')
    difference.fillna(0,inplace=True)
    difference.reset_index(inplace=True)

    aggs_cols.reset_index(inplace=True)

    credit_card_balance_features = pd.DataFrame()
    credit_card_balance_features['SK_ID_PREV'] = credit_card_balance['SK_ID_PREV']
    credit_card_balance_features['SK_ID_CURR'] = credit_card_balance['SK_ID_CURR']
    credit_card_balance_features.drop_duplicates(subset='SK_ID_PREV',inplace=True)

    credit_card_balance_features = credit_card_balance_features.merge(aggs_cols,on='SK_ID_PREV')
    credit_card_balance_features = credit_card_balance_features.merge(difference,on='SK_ID_PREV')

    fi = credit_card_balance_features.groupby('SK_ID_CURR')[credit_card_balance_features.columns[2:]].sum()
    fi.to_csv(path_save,sep=',')

main(path_read,path_save)