import pandas as pd

#Пути до файлов
path_read_bureau = 'E:\\home-credit-default-risk\\bureau.csv'
path_read_bb = 'E:\\home-credit-default-risk\\old_bureau_balance.csv'
path_save = 'E:\\home-credit-default-risk\\bureau_balance_features.csv'

def main(path_read_bureau, path_read_bb, path_save):

    #preprocessing
    bureau = pd.read_csv(path_read_bureau)
    bureau_balance = pd.read_csv(path_read_bb)
    bureau_balance_features = pd.DataFrame(bureau_balance.drop_duplicates('SK_ID_BUREAU'))[['SK_ID_BUREAU']]
    bureau_balance_features['SK_ID_CURR'] = bureau['SK_ID_CURR']

    #1. number of open credits
    status_table = bureau_balance.pivot_table(index="SK_ID_BUREAU",columns='STATUS',aggfunc='count').fillna(0)['MONTHS_BALANCE']
    bureau_balance_features = bureau_balance_features.merge(status_table,on='SK_ID_BUREAU')
    bureau_balance_features['NUMBER_OPEN_CREDITS'] = bureau_balance_features.loc[:,"0":"5"].sum(axis=1)

    #2. number of closed credits
    bureau_balance_features['NUMBER_CLOSED_CREDITS'] = bureau_balance_features['C']

    #3. number of overdue credits by days (уже посчитаны в колонках 1-5)
    bureau_balance_features = bureau_balance_features.rename(
    columns={
        '0': 'NO_DPD',
        '1': 'DPD_30',
        '2': 'DPD_60',
        '3': 'DPD_90',
        '4':'DPD_120',
        '5': 'DPD_120+_OR_SOLD'

        }
    )

    #4. number of credits
    credit = bureau_balance.groupby('SK_ID_BUREAU').agg(NUMBER_CREDITS = ('SK_ID_BUREAU','count'))
    bureau_balance_features = bureau_balance_features.merge(credit,on='SK_ID_BUREAU')

    #5.ratio of closed credits
    bureau_balance_features['CLOSED_CREDIT_RATIO'] = bureau_balance_features['NUMBER_CLOSED_CREDITS']/bureau_balance_features['NUMBER_CREDITS']

    #6. ratio of open credits
    bureau_balance_features['OPEN_CREDIT_RATIO'] = bureau_balance_features['NUMBER_OPEN_CREDITS']/bureau_balance_features['NUMBER_CREDITS']

    #7. ratio of overdue credits per days
    bureau_balance_features['DPD_30_PERCENT'] = bureau_balance_features['DPD_30']/bureau_balance_features.loc[:,'DPD_30':'DPD_120+_OR_SOLD'].sum(axis=1)
    bureau_balance_features['DPD_30_PERCENT'].fillna(0,inplace=True)

    bureau_balance_features['DPD_60_PERCENT'] = bureau_balance_features['DPD_30']/bureau_balance_features.loc[:,'DPD_30':'DPD_120+_OR_SOLD'].sum(axis=1)
    bureau_balance_features['DPD_60_PERCENT'].fillna(0,inplace=True)

    bureau_balance_features['DPD_90_PERCENT'] = bureau_balance_features['DPD_30']/bureau_balance_features.loc[:,'DPD_30':'DPD_120+_OR_SOLD'].sum(axis=1)
    bureau_balance_features['DPD_90_PERCENT'].fillna(0,inplace=True)

    bureau_balance_features['DPD_120_PERCENT'] = bureau_balance_features['DPD_30']/bureau_balance_features.loc[:,'DPD_30':'DPD_120+_OR_SOLD'].sum(axis=1)
    bureau_balance_features['DPD_120_PERCENT'].fillna(0,inplace=True)

    bureau_balance_features['DPD_120+_PERCENT'] = bureau_balance_features['DPD_30']/bureau_balance_features.loc[:,'DPD_30':'DPD_120+_OR_SOLD'].sum(axis=1)
    bureau_balance_features['DPD_120+_PERCENT'].fillna(0,inplace=True)

    #8. Interval btw last closed credit and current application
    interval = bureau_balance.loc[bureau_balance['STATUS'] == 'C'].groupby('SK_ID_BUREAU')[['MONTHS_BALANCE']].max().reset_index()
    interval.rename(columns={'MONTHS_BALANCE':'INTERVAL_LAST_CLOSED_CREDIT'},inplace=True)
    bureau_balance_features = bureau_balance_features.merge(interval,on='SK_ID_BUREAU')

    #9. interval btw last active loan and current application
    active_credits = bureau_balance.loc[(bureau_balance['MONTHS_BALANCE'] == -1)&(bureau_balance['STATUS'] != 'C')&(bureau_balance['STATUS'] != 'X')]
    bba = bureau_balance.merge(active_credits['SK_ID_BUREAU'], on = 'SK_ID_BUREAU')
    
    interval = bba.groupby('SK_ID_BUREAU')[['MONTHS_BALANCE']].min().reset_index()
    interval.rename(columns={'MONTHS_BALANCE':'INTERVAL_LAST_OPEN_CREDIT'},inplace=True)
    bureau_balance_features = bureau_balance_features.merge(interval,how = 'outer', on='SK_ID_BUREAU')
    
    fi = bureau_balance_features.groupby('SK_ID_CURR')[bureau_balance_features.columns[2:]].sum()
    fi.to_csv(path_save,sep=',')

main(path_read_bureau,path_read_bb,path_save)
    