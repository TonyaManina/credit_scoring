import pandas as pd

#Пути до файлов
path_read = 'E:\\home-credit-default-risk\\previous_application.csv'
path_save = 'E:\\home-credit-default-risk\\previous_application_features.csv'

def int_rate_definer(x):
    #cringe
    if x <= 15:
        return 'Low'
    elif x <= 25:
        return 'Medium'
    else:
        return 'Huge' 

def main(path_read,path_save):
    #preprocessing
    previous_application = pd.read_csv(path_read)
    previous_application_features = pd.DataFrame()
    previous_application_features['SK_ID_CURR'] = previous_application['SK_ID_CURR']
    previous_application_features['SK_ID_PREV'] = previous_application['SK_ID_PREV']

    #1. amt_application/amt_credit
    previous_application_features['AMT_APPLICATION_PER_AMT_CREDIT'] = previous_application['AMT_APPLICATION'] / previous_application['AMT_CREDIT']

    #2. amt_credit/amt_goods
    previous_application_features['AMT_CREDIT_PER_AMT_GOODS'] = previous_application['AMT_CREDIT']/previous_application['AMT_GOODS_PRICE']
    previous_application_features = previous_application_features.groupby('SK_ID_CURR')[['SK_ID_CURR','AMT_APPLICATION_PER_AMT_CREDIT','AMT_APPLICATION_PER_AMT_CREDIT']].transform('mean')

    #3. approval ratio
    new = pd.get_dummies(previous_application, columns=['NAME_CONTRACT_STATUS'], drop_first= False, dtype=float)
    cols = ['SK_ID_CURR','NAME_CONTRACT_STATUS_Approved']
    approval_ratio = new.groupby('SK_ID_CURR')[cols].transform('mean')
    approval_ratio.rename(columns={
        'NAME_CONTRACT_STATUS_Approved':'APPROVAL_RATIO'
    },inplace=True)
    previous_application_features = previous_application_features.merge(approval_ratio,on='SK_ID_CURR')

    #4. mean amount per status
    #mean_amt_status = previous_application.groupby(['SK_ID_CURR','NAME_CONTRACT_STATUS'])[['SK_ID_CURR','AMT_CREDIT']].transform('mean')
    #mean_amt_status.rename(columns={
    #'AMT_CREDIT':'MEAN_AMT_CREDIT_PER_STATUS'
    #},inplace=True)
    #previous_application_features =previous_application_features.merge(mean_amt_status,on='SK_ID_CURR')

    #5. custom score
    new['INT_RATE_CUSTOM'] = new['RATE_INTEREST_PRIMARY'].apply(int_rate_definer)
    mean_approval = new.groupby('INT_RATE_CUSTOM')[['NAME_CONTRACT_STATUS_Approved']].transform('mean')
    mean_approval.rename(columns={
    'NAME_CONTRACT_STATUS_Approved':'CUSTOM_SCORE'
    },inplace=True)
    previous_application_features['CUSTOM_SCORE'] = mean_approval

    #6. Some aggregates
    cols_agg = {
    'AMT_CREDIT': ['min','max','mean'],
    'AMT_GOODS_PRICE': ['min','max','mean'],
    'CNT_PAYMENT': ['min','max','mean'],
    }
    strange_feat = previous_application.groupby('SK_ID_CURR').agg({**cols_agg})
    strange_feat.columns = [f"{col[0]}_{col[1]}" for col in strange_feat.columns]
    previous_application_features =previous_application_features.merge(strange_feat,on='SK_ID_CURR')

    cols = ['SK_ID_CURR', 'AMT_APPLICATION_PER_AMT_CREDIT',
       'AMT_APPLICATION_PER_AMT_CREDIT', 'APPROVAL_RATIO', 'CUSTOM_SCORE',
       'AMT_CREDIT_min', 'AMT_CREDIT_max', 'AMT_CREDIT_mean',
       'AMT_GOODS_PRICE_min', 'AMT_GOODS_PRICE_max', 'AMT_GOODS_PRICE_mean',
       'CNT_PAYMENT_min', 'CNT_PAYMENT_max', 'CNT_PAYMENT_mean']
    previous_application_features = pd.DataFrame(previous_application_features.drop_duplicates("SK_ID_CURR"))[cols]

    previous_application_features.set_index('SK_ID_CURR',inplace=True)
    previous_application_features.to_csv(path_save,sep=',')

main(path_read,path_save)



