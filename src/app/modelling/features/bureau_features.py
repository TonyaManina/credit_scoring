import pandas as pd

#Пути до файлов
path_read = 'E:\\home-credit-default-risk\\bureau.csv'
path_save = 'E:\\home-credit-default-risk\\bureau_features.csv'

def main(path_read,path_save):

    #preprocessing
    bureau = pd.read_csv(path_read)
    bureau_features = pd.DataFrame()

    #1. Max debt sum
    bureau_features = bureau.groupby('SK_ID_CURR').agg(MAX_DEBT_AMT = ('AMT_CREDIT_SUM_DEBT','max')).reset_index()

    #2. Min debt sum
    min_debt_sum = bureau.groupby('SK_ID_CURR').agg(MIN_DEBT_AMT = ('AMT_CREDIT_SUM_DEBT','max')).reset_index()
    bureau_features['MIN_DEBT_AMT'] = min_debt_sum['MIN_DEBT_AMT']

    #3. какую долю от суммы активного кредита просрочил
    bureau_active = bureau.loc[bureau['CREDIT_ACTIVE'] == 'Active']
    feature = pd.DataFrame()
    feature['SK_ID_CURR'] = bureau_active['SK_ID_CURR']
    feature['PERSENT_ACTIVE_DEBT'] = bureau_active['AMT_CREDIT_SUM_DEBT']/bureau_active['AMT_CREDIT_SUM']
    feature = feature.groupby('SK_ID_CURR').agg(PERSENT_OF_DEBT_ACTIVE = ('PERSENT_ACTIVE_DEBT','max')).reset_index()
    bureau_features = bureau_features.merge(feature,how='left')

    #4. Number of credits of different types
    type_of_credit = pd.pivot_table(bureau,index='SK_ID_CURR',columns=['CREDIT_TYPE'],values='DAYS_CREDIT_UPDATE',aggfunc='count',fill_value=0,).reset_index().add_prefix('count_')
    bureau_features = bureau_features.merge(type_of_credit,left_on='SK_ID_CURR',right_on='count_SK_ID_CURR')
    bureau_features.drop('count_SK_ID_CURR',axis=1,inplace=True)

    #5. Number of overdue credits of dif types
    bureau['FLAG_OVERDUE'] = 0
    bureau.loc[bureau['CREDIT_DAY_OVERDUE'] > 0,'FLAG_OVERDUE'] = 1
    type_of_credit_overdue = pd.pivot_table(bureau,index='SK_ID_CURR',columns='CREDIT_TYPE',values='FLAG_OVERDUE',aggfunc='sum',fill_value=0,).reset_index().add_prefix('count_overdue_')
    bureau_features = bureau_features.merge(type_of_credit_overdue,left_on='SK_ID_CURR',right_on='count_overdue_SK_ID_CURR')
    bureau_features.drop('count_overdue_SK_ID_CURR',axis=1,inplace=True)

    #6. Number of closed credits of dif types
    bureau['FLAG_CLOSED'] = 1
    bureau.loc[bureau['DAYS_ENDDATE_FACT'].isna(),'FLAG_CLOSED'] = 0
    type_of_credit_closed = pd.pivot_table(bureau,index='SK_ID_CURR',columns='CREDIT_TYPE',values='FLAG_CLOSED',aggfunc='sum',fill_value=0,).reset_index().add_prefix('count_closed_')
    bureau_features = bureau_features.merge(type_of_credit_closed,left_on='SK_ID_CURR',right_on='count_closed_SK_ID_CURR')
    bureau_features.drop('count_closed_SK_ID_CURR',axis=1,inplace=True)

    bureau_features.set_index('SK_ID_CURR',inplace=True)
    bureau_features.to_csv(path_save,sep=',')

main(path_read,path_save)    
