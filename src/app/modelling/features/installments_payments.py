import pandas as pd

#Пути до файлов
path_read = 'E:\\home-credit-default-risk\\old_installment_payments.csv'
path_save = 'E:\\home-credit-default-risk\\installments_payments_features.csv'

def main(path_read,path_save):
    #скудновато с признаками, но я уже умирала, простите...
    #Preprocessing
    installments_payments = pd.read_csv(path_read)
    installments_payments_features = pd.DataFrame()
    installments_payments_features['SK_ID_PREV'] = installments_payments['SK_ID_PREV']
    installments_payments_features['SK_ID_CURR'] = installments_payments['SK_ID_CURR']

    #features
    installments_payments_features['DIFF_DAYS'] = installments_payments['DAYS_INSTALMENT'] - installments_payments['DAYS_ENTRY_PAYMENT']
    installments_payments_features['DIFF_PAYMENT'] = installments_payments['AMT_PAYMENT'] - installments_payments['AMT_INSTALMENT']

    installments_payments_features['FLAG_DPD'] = installments_payments_features.loc[:,'DIFF_DAYS'].apply(lambda x: 0 if x >= 0 else 1)
    installments_payments_features['FLAG_DEBT'] = installments_payments_features.loc[:,'DIFF_PAYMENT'].apply(lambda x: 0 if x >= 0 else 1)

    smth = pd.DataFrame()
    smth['RATIO_DPD'] = installments_payments_features.groupby('SK_ID_PREV')['FLAG_DPD'].sum()/installments_payments_features.groupby('SK_ID_PREV')['FLAG_DPD'].count()
    smth['RATIO_DEBT'] = installments_payments_features.groupby('SK_ID_PREV')['FLAG_DEBT'].sum()/installments_payments_features.groupby('SK_ID_PREV')['FLAG_DEBT'].count()
    installments_payments_features = installments_payments_features.merge(smth,on='SK_ID_PREV')

    fi = installments_payments_features.groupby('SK_ID_CURR')[installments_payments_features.columns[2:]].mean()
    fi.to_csv(path_save,sep=',')

main(path_read,path_save)
    
  