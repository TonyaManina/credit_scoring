import pandas as pd
import os
import sys
import json
from dataclasses import dataclass

@dataclass
class PosCashBalanceIDs:
    SK_ID_PREV: int
    SK_ID_CURR: int
    NAME_CONTRACT_STATUS: str

@dataclass
class AmtCredit:
    CREDIT_CURRENCY: str
    AMT_CREDIT_MAX_OVERDUE: float
    AMT_CREDIT_SUM: float
    AMT_CREDIT_SUM_DEBT: float
    AMT_CREDIT_SUM_LIMIT: float
    AMT_CREDIT_SUM_OVERDUE: float
    AMT_ANNUITY: float

def POS_CASH_parser(data):

    #df from data: 2 cols: list of jsons and CNT_INSTALMENT
    initial_df = pd.DataFrame(data)
    initial_df = initial_df.explode('records')
    initial_df = initial_df.reset_index(drop=True)

    df_to_parse = pd.json_normalize(initial_df['records'])

    df_to_parse['PosCashBalanceIDs'] = df_to_parse['PosCashBalanceIDs'].apply(lambda x: eval(x))

    df_to_parse['SK_ID_PREV'] = df_to_parse['PosCashBalanceIDs'].apply(lambda x: x.SK_ID_PREV)
    df_to_parse['SK_ID_CURR'] = df_to_parse['PosCashBalanceIDs'].apply(lambda x: x.SK_ID_CURR)
    df_to_parse['NAME_CONTRACT_STATUS'] = df_to_parse['PosCashBalanceIDs'].apply(lambda x: x.NAME_CONTRACT_STATUS)

    df_to_parse.drop(axis=1,labels='PosCashBalanceIDs',inplace=True)

    df_pos_cash = pd.concat([initial_df,df_to_parse],axis=1)
    df_pos_cash.drop(axis=1,labels='records',inplace=True)

    df_pos_cash = df_pos_cash[['SK_ID_PREV','SK_ID_CURR','MONTHS_BALANCE','CNT_INSTALMENT','CNT_INSTALMENT_FUTURE','NAME_CONTRACT_STATUS','SK_DPD','SK_DPD_DEF']]
    return df_pos_cash


def bureau_parser(data):

    initial_df = pd.DataFrame(data)

    df_to_parse = pd.json_normalize(initial_df['record'])
    df_to_parse['AmtCredit'] = df_to_parse['AmtCredit'].apply(lambda x: eval(x))

    df_to_parse['CREDIT_CURRENCY'] = df_to_parse['AmtCredit'].apply(lambda x: x.CREDIT_CURRENCY)
    df_to_parse['AMT_CREDIT_MAX_OVERDUE'] = df_to_parse['AmtCredit'].apply(lambda x: x.AMT_CREDIT_MAX_OVERDUE)
    df_to_parse['AMT_CREDIT_SUM'] = df_to_parse['AmtCredit'].apply(lambda x: x.AMT_CREDIT_SUM)
    df_to_parse['AMT_CREDIT_SUM_DEBT'] = df_to_parse['AmtCredit'].apply(lambda x: x.AMT_CREDIT_SUM_DEBT)
    df_to_parse['AMT_CREDIT_SUM_LIMIT'] = df_to_parse['AmtCredit'].apply(lambda x: x.AMT_CREDIT_SUM_LIMIT)
    df_to_parse['AMT_ANNUITY'] = df_to_parse['AmtCredit'].apply(lambda x: x.AMT_ANNUITY)
    df_to_parse['AMT_CREDIT_SUM_OVERDUE'] = df_to_parse['AmtCredit'].apply(lambda x: x.AMT_CREDIT_SUM_OVERDUE)

    df_to_parse.drop(axis=1,labels='AmtCredit',inplace=True)

    bureau_df = pd.concat([initial_df,df_to_parse],axis=1)
    bureau_df.drop(axis=1,labels='record',inplace=True)
    bureau_df = bureau_df[['SK_ID_CURR','SK_ID_BUREAU','CREDIT_ACTIVE','CREDIT_CURRENCY','DAYS_CREDIT',
                           'CREDIT_DAY_OVERDUE','DAYS_CREDIT_ENDDATE','DAYS_ENDDATE_FACT','AMT_CREDIT_MAX_OVERDUE',
                           'CNT_CREDIT_PROLONG','AMT_CREDIT_SUM','AMT_CREDIT_SUM_DEBT','AMT_CREDIT_SUM_LIMIT',
                           'AMT_CREDIT_SUM_OVERDUE','CREDIT_TYPE','DAYS_CREDIT_UPDATE','AMT_ANNUITY']]
    return bureau_df


def main(path_to_log,path_to_csv1,path_to_csv2):
    #log_path = input("please enter a full log path")
    #log_path = 'E:/cft/POS_CASH_balance_plus_bureau-001.log'

    data = [] #initial dict
    with open(path_to_log) as f:
        for line in f:
            data.append(json.loads(line))

    #loading data to lists
    bureau_list = []
    pos_cash_list = []
    for i in range(len(data)):
        if data[i]['type'] == 'bureau':
            bureau_list.append(data[i]['data'])
        elif data[i]['type'] == 'POS_CASH_balance':
            pos_cash_list.append(data[i]['data'])

    #POS_CASH
    pos_cash = POS_CASH_parser(pos_cash_list)

    #Bureau
    bureau = bureau_parser(bureau_list)
    #path_to_csvs = input("please enter a full path to a folder where you want to save your tables")
    #path_to_csvs = 'E:/cft'
    pos_cash.to_csv(path_to_csv1+'/POS_CASH_balance_parsed.csv',sep=',', index = False)
    bureau.to_csv(path_to_csv2+'/bureau_parsed.csv',sep=',',index = False)


path_to_csv1 = 'E:/cft'
path_to_csv2 = 'E:/cft'
path_to_log = 'E:/cft/POS_CASH_balance_plus_bureau-001.log'
main(path_to_log,path_to_csv1,path_to_csv2)





